# Pârâu

## Some glue for event streams

### Scope

- Read events from a kafka stream
- Allow implementing some basic logic on events
  - Filtering
  - Aggregation
  - If-this-then-that logic
- Send HTTP requests based on this logic

### Dependencies

- Kafka
- Flink
- Java

### Installation

To run the kafka/flink stack locally:

```bash
> docker-compose build
> docker-compose up
```

To build the flink job:

```bash
mvn package -f "./parau/pom.xml"
```
