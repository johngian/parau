package org.parau;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.connector.kafka.source.reader.deserializer.KafkaRecordDeserializationSchema;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.RichCoFlatMapFunction;
import org.apache.flink.streaming.util.serialization.JSONKeyValueDeserializationSchema;
import org.apache.flink.util.Collector;

public class StreamingJob {

	public static void main(String[] args) throws Exception {
		// set up the streaming execution environment
		final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

		ParameterTool parameter = ParameterTool.fromArgs(args);

		KafkaSource<ObjectNode> configSource = KafkaSource.<ObjectNode>builder()
				.setBootstrapServers(parameter.getRequired("kafkaBootstrapServer"))
				.setTopics(parameter.getRequired("kafkaConfigTopic"))
				.setGroupId(parameter.getRequired("kafkaGroupID"))
				.setStartingOffsets(OffsetsInitializer.latest())
				.setDeserializer(KafkaRecordDeserializationSchema.of(new JSONKeyValueDeserializationSchema(true)))
				.build();

		KafkaSource<ObjectNode> eventSource = KafkaSource.<ObjectNode>builder()
				.setBootstrapServers(parameter.getRequired("kafkaBootstrapServer"))
				.setTopics(parameter.getRequired("kafkaEventTopic"))
				.setGroupId(parameter.getRequired("kafkaGroupID"))
				.setStartingOffsets(OffsetsInitializer.latest())
				.setDeserializer(KafkaRecordDeserializationSchema.of(new JSONKeyValueDeserializationSchema(true)))
				.build();

		DataStream<ObjectNode> configStream = env
				.fromSource(configSource, WatermarkStrategy.noWatermarks(), "Kafka Source - Configuration")
				.keyBy(value -> value.get("value").get("schemaID"));

		DataStream<ObjectNode> eventStream = env
				.fromSource(eventSource, WatermarkStrategy.noWatermarks(), "Kafka source - Events")
				.keyBy(value -> value.get("value").get("schemaID"));

		DataStream<Tuple2<ObjectNode, ObjectNode>> enrichedStream = eventStream.connect(configStream)
				.flatMap(new EnrichmentFunction());

		enrichedStream.print();

		// execute program
		env.execute("Parau streaming data job");
	}

	public static class EnrichmentFunction
			extends RichCoFlatMapFunction<ObjectNode, ObjectNode, Tuple2<ObjectNode, ObjectNode>> {
		private transient ValueState<ObjectNode> configState;

		@Override
		public void open(Configuration config) {
			configState = getRuntimeContext().getState(new ValueStateDescriptor<>("Saved config", ObjectNode.class));
		}

		@Override
		public void flatMap1(ObjectNode event, Collector<Tuple2<ObjectNode, ObjectNode>> out) throws Exception {
			ObjectNode config = configState.value();
			if (config != null) {
				out.collect(Tuple2.of(config, event));
			}
		}

		@Override
		public void flatMap2(ObjectNode config, Collector<Tuple2<ObjectNode, ObjectNode>> out) throws Exception {
			configState.update(config);
		}
	}
}
