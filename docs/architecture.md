# Architecture

## Sequence diagram

```mermaid
sequenceDiagram
    participant configuration as User Configuration
    participant origin as Origin
    participant ingest as HTTP Ingest
    participant validator as JSON schema validator
    participant kafka as Kafka
    participant flink as Flink
    participant flinkjob as Flink Job
    participant sink as HTTP Sink
    origin->>ingest: New event
    ingest->>validator: Validate schema

    alt valid event
        validator->>ingest: Event is valid
        ingest->>origin: Success
        ingest->>kafka: Produce message
    else invalid event
        validator->>ingest: Event is invalid
        ingest->>origin: Failure
    end

    kafka->>flink: Event stream
    flink->>flinkjob: Process event object
    flinkjob->>flinkjob: Test message for logic block
    alt logic block condition true
        flinkjob->>sink: Send HTTP request to destination
    end

    configuration->>kafka: Configuration create/update
    kafka->>flink: Config stream
    flink->>flinkjob: Enrich stream with latest config
```

## Primitives

### Handler

By handler we refer to the way users want the events to be used.
Current scope:

* Event field matching

#### Configuration

* Handler ID
  * Identifier for the handler (required)
* Event schema
  * Match events that use a schema (required)
* Field matching
  * List of key/value
  * Boolean operator
* HTTP destination
  * URL of the HTTP endpoint
  * Method
    * GET
    * POST
  * Body
    * Simple form
    * JSON
  * Payload template
    * A template to be used as a base for requests.

### Event

By event we refer to the input received from different sources and after validation are published in a kafka topic.

### Event schema

By event schema we refer to the schema (based on JSON schema) that describes the structure of the events submitted.
